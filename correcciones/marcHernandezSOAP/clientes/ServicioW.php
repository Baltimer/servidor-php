<?php

class ServicioW extends SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl = 'serviciow.wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      
      parent::__construct($wsdl, $options);
    }

    /**
     * Obtener PVP de un producto  Recibe como parámetro el código de un producto, y devuelve el PVP correspondiente al mismo.
     *
     * @param string $product_code
     * @access public
     * @return string
     */
    public function getPVP($product_code)
    {
      return $this->__soapCall('getPVP', array($product_code));
    }

    /**
     * Obtener stock de un producto en una tienda  Recibe como parámetros el código de un producto y el código de una tienda, y devuelve el stock existente en dicha tienda del producto.
     *
     * @param string $product_code
     * @param string $shop_code
     * @access public
     * @return string
     */
    public function getStock($product_code, $shop_code)
    {
      return $this->__soapCall('getStock', array($product_code, $shop_code));
    }

    /**
     * Obtener familias  No recibe parametros y devuelve un array con los codigos de todas las familias existentes.
     *
     * @access public
     * @return UNKNOWN
     */
    public function getFamilias()
    {
      return $this->__soapCall('getFamilias', array());
    }

    /**
     * Obtener productos de una familia  Recibe como parámetro el código de una familia y devuelve un array con los códigos de todos los productos de esa familia.
     *
     * @param string $family_code
     * @access public
     * @return UNKNOWN
     */
    public function getProductosFamilia($family_code)
    {
      return $this->__soapCall('getProductosFamilia', array($family_code));
    }

}
