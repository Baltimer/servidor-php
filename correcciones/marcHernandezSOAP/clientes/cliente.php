<?php

    $url="http://localhost:8080/servicio.php";
    $uri="http://localhost:8080/";
    $cliente = new SoapClient(null, array('location'=>$url,'uri'=>$uri));

    $product_code = "IXUS115HSAZ";
    $shop_code = "2";
    $codigo_familia = "CAMARA";

    $pvp_product = $cliente->getPVP($product_code);
    $stock_product = $cliente->getStock($product_code, $shop_code);
    $familias = $cliente->getFamilias();
    $productosfamilia = $cliente->getProductosFamilia($codigo_familia);


    // Realice una llamada a cada una de las funciones programadas y muestre el resultado obtenido.
    echo "El PVP del producto con codigo '" . $product_code . "' es: " . $pvp_product . "<br/>";

    $real_stock_product = $stock_product ? $stock_product : 0;
    echo "El stock del producto con codigo '" . $product_code . "' es: " . $real_stock_product . "<br/>";


    echo "Los codigos de las familias existentes son: ". implode(', ', $familias) . "<br/>";

    echo "Los codigos de los productos de la familia con codigo '" . $codigo_familia . "' son: " . implode(', ', $productosfamilia) . "<br/>";
?>