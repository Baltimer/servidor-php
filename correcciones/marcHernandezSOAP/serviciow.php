<?php

class ServicioW
{
    /**
     * Connection to Database
     *
     * This method allows to connect to the database and return connection.
     *
     * @return PDO
     */
    private function db_connect()
    {
        $server = 'localhost';
        $user = 'root';
        $pass = '1234';
        $db = 'dwes';
        try {
            $link = new PDO("mysql:host=" . $server . ";dbname=" . $db, $user, $pass);
            return $link;
        } catch (PDOException $e) {
            die("Could not connect to the MySQL server at localhost: " . $e->getMessage());
        }
    }


    /**
     * Obtener PVP de un producto
     *
     * Recibe como parámetro el código de un producto, y devuelve el PVP correspondiente al mismo.
     *
     * @param string $product_code
     * @return string
     */
    function getPVP($product_code)
    {
        $strSQL = sprintf("SELECT PVP FROM producto where cod = '%s';", $product_code);
        $link = $this->db_connect();
        $data = $link->query($strSQL);
        $row = $data->fetch();
        return (string) $row['PVP'];
    }


    /**
     * Obtener stock de un producto en una tienda
     *
     * Recibe como parámetros el código de un producto y el código de una tienda, y devuelve el stock
     * existente en dicha tienda del producto.
     *
     * @param string $product_code
     * @param string $shop_code
     * @return string
     */
    function getStock($product_code, $shop_code)
    {
        $strSQL = sprintf("SELECT unidades FROM stock WHERE producto = '%s' and tienda = %u;", $product_code, $shop_code);
        $link = $this->db_connect();
        $data = $link->query($strSQL);
        $row = $data->fetch();
        return (string) $row['unidades'];
    }


    /**
     * Obtener familias
     *
     * No recibe parametros y devuelve un array con los codigos de todas las familias existentes.
     *
     * @return array
     */
    function getFamilias()
    {
        $strSQL = "SELECT cod FROM familia;";
        $link = $this->db_connect();
        $data = $link->query($strSQL);
        $row = $data->fetchAll(PDO::FETCH_COLUMN, 0);
        return $row;
    }


    /**
     * Obtener productos de una familia
     *
     * Recibe como parámetro el código de una familia y devuelve un array con los códigos de todos
     * los productos de esa familia.
     *
     * @param string $family_code
     * @return array
     */
    function getProductosFamilia($family_code)
    {
        $strSQL = sprintf("SELECT cod FROM producto WHERE familia =  '%s';", $family_code);
        $link = $this->db_connect();
        $data = $link->query($strSQL);
        $row = $data->fetchAll(PDO::FETCH_COLUMN, 0);
        return $row;
    }
}

$server = new SoapServer("serviciow.wsdl");

$server->setClass('ServicioW');
$server->handle();

?>