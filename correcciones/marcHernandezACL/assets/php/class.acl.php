<?php


/**
 * Class ACL
 */
class ACL
	{
    var $link;
    var $perms = array();		//Array : Stores the permissions for the user
    var $userID = 0;			//Integer : Stores the ID of the current user
    var $userRoles = array();	//Array : Stores the roles of the current user

        /** ACL constructor.
         * @param string $userID
         */
        function __constructor($userID = '')
		{
			if ($userID != '')
			{
				$this->userID = floatval($userID);
			} else {
				$this->userID = floatval($_SESSION['userID']);
			}
			$this->link = db_connect();
			$this->userRoles = $this->getUserRoles('ids');
			$this->buildACL();
		}

    /**
     * ACL constructor
     *
     * This method is the ACL constructor.
     *
     * @param string $userID
     */
    function ACL($userID = '')
		{
			$this->__constructor($userID);
			//crutch for PHP4 setups
		}

    /** ACL Builder
     *
     * This method get the rules for the user's role and then get individual user permissions.
     *
     */
    function buildACL()
		{
			//first, get the rules for the user's role
			if (count($this->userRoles) > 0)
			{
				$this->perms = array_merge($this->perms,$this->getRolePerms($this->userRoles));
			}
			//then, get the individual user permissions
			$this->perms = array_merge($this->perms,$this->getUserPerms($this->userID));
		}

    /**
     * Permission key getter
     *
     * This method allows to get permission keys from permission ID (permID) querying the database.
     *
     * @param $permID
     * @return mixed
     */
    function getPermKeyFromID($permID)
		{
			$strSQL = "SELECT `permKey` FROM `permissions` WHERE `ID` = " . floatval($permID) . " LIMIT 1";
			$data = $this->link->query($strSQL);
			$row = $data->fetch(PDO::FETCH_BOTH);
			return $row[0];
		}

    /**
     * Permission name getter
     *
     * This method allows to get permission name from permission ID (permID) querying the database.
     *
     * @param $permID
     * @return mixed
     */
    function getPermNameFromID($permID)
		{
			$strSQL = "SELECT `permName` FROM `permissions` WHERE `ID` = " . floatval($permID) . " LIMIT 1";
			$data = $this->link->query($strSQL);
			$row = $data->fetch(PDO::FETCH_BOTH);
			return $row[0];
		}

    /**
     * Role name getter
     *
     * This method allows to get role name from role ID (roleID) querying the database.
     *
     * @param $roleID
     * @return mixed
     */
    function getRoleNameFromID($roleID)
		{
			$strSQL = "SELECT `roleName` FROM `roles` WHERE `ID` = " . floatval($roleID) . " LIMIT 1";
			$data = $this->link->query($strSQL);
			$row = $data->fetch(PDO::FETCH_BOTH);
			return $row[0];
		}

    /**
     * User roles getter
     *
     * This method allows to retrieve user roles of the object querying the database.
     *
     * @return array
     */
    function getUserRoles()
		{
			$strSQL = "SELECT * FROM `user_roles` WHERE `userID` = " . floatval($this->userID) . " ORDER BY `addDate` ASC";
			$data = $this->link->query($strSQL);
			$resp = array();
			while($row = $data->fetch(PDO::FETCH_BOTH))
			{
				$resp[] = $row['roleID'];
			}
			return $resp;
		}

    /**
     * Roles getter
     *
     * This method allows to know all existing roles querying the database.
     *
     * @param string $format
     * @return array
     */
    function getAllRoles($format='ids')
		{
			$format = strtolower($format);
			$strSQL = "SELECT * FROM `roles` ORDER BY `roleName` ASC";
			$data = $this->link->query($strSQL);
			$resp = array();
			while($row = $data->fetch(PDO::FETCH_BOTH))
			{
				if ($format == 'full')
				{
					$resp[] = array("ID" => $row['ID'],"Name" => $row['roleName']);
				} else {
					$resp[] = $row['ID'];
				}
			}
			return $resp;
		}

    /**
     * Permissions getter
     *
     * This method allows to know all existing permissions querying the database.
     *
     * @param string $format
     * @return array
     */
    function getAllPerms($format='ids')
		{
			$format = strtolower($format);
			$strSQL = "SELECT * FROM `permissions` ORDER BY `permName` ASC";
			$data = $this->link->query($strSQL);
			$resp = array();
			while($row = $data->fetch(PDO::FETCH_ASSOC))
			{
				if ($format == 'full')
				{
					$resp[$row['permKey']] = array('ID' => $row['ID'], 'Name' => $row['permName'], 'Key' => $row['permKey']);
				} else {
					$resp[] = $row['ID'];
				}
			}
			return $resp;
		}

    /**
     * Role permissions getter
     *
     * This method allows to retrieve role permissions from userID querying the database.
     *
     * @param $role
     * @return array
     */
    function getRolePerms($role)
		{
			if (is_array($role))
			{
				$roleSQL = "SELECT * FROM `role_perms` WHERE `roleID` IN (" . implode(",",$role) . ") ORDER BY `ID` ASC";
			} else {
				$roleSQL = "SELECT * FROM `role_perms` WHERE `roleID` = " . floatval($role) . " ORDER BY `ID` ASC";
			}
			$data = $this->link->query($roleSQL);
			$perms = array();
			while($row = $data->fetch(PDO::FETCH_ASSOC))
			{
				$pK = strtolower($this->getPermKeyFromID($row['permID']));
				if ($pK == '') { continue; }
				if ($row['value'] === '1') {
					$hP = true;
				} else {
					$hP = false;
				}
				$perms[$pK] = array('perm' => $pK,'inheritted' => true,'value' => $hP,'Name' => $this->getPermNameFromID($row['permID']),'ID' => $row['permID']);
			}
			return $perms;
		}

    /**
     * User permissions getter
     *
     * This method allows to retrieve user permissions from userID querying the database.
     *
     * @param $userID
     * @return array
     */
    function getUserPerms($userID)
		{
			$strSQL = "SELECT * FROM `user_perms` WHERE `userID` = " . floatval($userID) . " ORDER BY `addDate` ASC";
			$data = $this->link->query($strSQL);
			$perms = array();
			while($row = $data->fetch(PDO::FETCH_ASSOC))
			{
				$pK = strtolower($this->getPermKeyFromID($row['permID']));
				if ($pK == '') { continue; }
				if ($row['value'] == '1') {
					$hP = true;
				} else {
					$hP = false;
				}
				$perms[$pK] = array('perm' => $pK,'inheritted' => false,'value' => $hP,'Name' => $this->getPermNameFromID($row['permID']),'ID' => $row['permID']);
			}
			return $perms;
		}

    /**
     * User has role
     *
     * This method allows to know if a user has a role or not.
     *
     * @param $roleID
     * @return bool
     */
    function userHasRole($roleID)
		{
			foreach($this->userRoles as $k => $v)
			{
				if (floatval($v) === floatval($roleID))
				{
					return true;
				}
			}
			return false;
		}

    /**
     * Has permission
     *
     * This method allows to know if a permission exist or not.
     *
     * @param $permKey
     * @return bool
     */
    function hasPermission($permKey)
		{
			$permKey = strtolower($permKey);
			if (array_key_exists($permKey,$this->perms))
			{
				if ($this->perms[$permKey]['value'] === '1' || $this->perms[$permKey]['value'] === true)
				{
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

    /**
     * Username getter
     *
     * This method allows to retrieve username from userID querying the database.
     *
     * @param $userID
     * @return mixed
     */
    function getUsername($userID)
		{
			$strSQL = "SELECT `username` FROM `users` WHERE `ID` = " . floatval($userID) . " LIMIT 1";
			$data = $this->link->query($strSQL);
			$row = $data->fetch(PDO::FETCH_BOTH);
			return $row[0];
		}
	}

?>