<?php
session_start();
ob_start();


/**
 * Connection to Database
 *
 * This method allows to connect to the database and return connection.
 *
 * @return PDO
 */
function db_connect() {
    $server = 'localhost';
    $user = 'root';
    $pass = '';
    $db = 'acl_test';
    try {
        $link = new PDO("mysql:host=" . $server . ";dbname=" . $db , $user, $pass);
        return $link;
    } catch (PDOException $e) {
        die("Could not connect to the MySQL server at localhost: " . $e->getMessage());
    }
}

?>