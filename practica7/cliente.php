<?php
require_once 'servicio.php';

$servicio = new Servicio();
echo "Función <b><i>getPVP</b></i>: ";
echo $servicio->getPVP('3DSNG') . '€';
echo "<br>";

echo "Función <b><i>getStock</b></i>: ";
echo $servicio->getStock('3DSNG', 1) . 'unidades';
echo "<br>";

echo "Función <b><i>getFamilias</b></i>: ";
$contador = 0;
foreach($servicio->getFamilias() as $familia){
   echo ($contador != count($servicio->getFamilias()) - 1) ? $familia->cod . ' - ' : $familia->cod;
   $contador++;
}
echo "<br>";

echo "Función <b><i>getProductosFamilia</b></i>: ";
$contador = 0;
foreach($servicio->getProductosFamilia('NETBOK') as $producto){
   echo ($contador != count($servicio->getProductosFamilia('NETBOK')) - 1) ? $producto->cod . ' - ' : $producto->cod;
   $contador++;
}
echo "<br>";
