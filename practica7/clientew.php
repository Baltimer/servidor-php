<?php

$cliente = new SoapClient('serviciow.wsdl');

try{
    // Función getPVP con SOAP
    $response = $cliente->getPVP('3DSNG');
    echo 'Función SOAP <b><i>getPVP</i></b>: ' . $response . '€';
    echo "<br>";

    // Función getStock con SOAP
    $response = $cliente->getStock('3DSNG', 1);
    echo 'Función SOAP <b><i>getPVP</i></b>: ' . $response . 'unidades';
    echo "<br>";

    // Función getFamilias con SOAP
    $contador = 0;
    echo 'Función SOAP <b><i>getFamilias</i></b>: ';
    $familias = $cliente->getFamilias();
    foreach($familias as $familia){
        echo ($contador != count($familias) - 1) ? $familia->cod . ' - ' : $familia->cod;
        $contador++;
    }
    echo "<br>";

    // Función getProductosFamilia con SOAP
    $contador = 0;
    echo 'Función SOAP <b><i>getProductosFamilia</i></b>: ';
    $productos = $cliente->getProductosFamilia('NETBOK');
    foreach($productos as $producto){
        echo 'Función SOAP <b><i>getPVP</i></b>: ' . ($contador != count($productos) - 1) ? $producto->cod . ' - ' : $producto->cod;
        $contador++;
    }
    echo "<br>";
} catch(SoapFault $e){
    echo "<br>";
    print($e->getMessage());
}
