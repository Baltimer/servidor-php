<?php
require_once 'conexion.php';

class Servicio{

    /**
     * @var Conexion contiene un objeto conexión
     */
    private $conexion = null;
    
    function __construct(){
        $conexion = new Conexion();
        $this->conexion = $conexion;
    }

    /**
     * Función que devuelve el precio de un producto
     *
     * @param string $codigo código del producto
     * 
     * @return integer precio del producto
     */
    public function getPVP($codigo){
        $resultado = $this->conexion->select("SELECT PVP FROM producto WHERE cod = '$codigo'");
        $precio = (count($resultado) > 0)  ? $resultado[0]->PVP : 0;
        return $precio;
    }
    
    /**
     * Función que devuelve el stock de un producto en una tienda
     *
     * @param string $codProducto código del producto
     * @param string $codTienda código de la tienda
     * 
     * @return integer devuelve el stock disponible
     */
    public function getStock($codProducto, $codTienda){
        $resultado = $this->conexion->select("SELECT unidades FROM stock 
                                        WHERE tienda = '$codTienda' AND producto = '$codProducto'");
        $stock = (count($resultado) > 0) ? $resultado[0]->unidades : 0;
        return $stock;
    }

    /**
     * Función que devuelve las familias
     * 
     * @param 
     * @return array array que contiene las familias
     */
    public function getFamilias(){
        $resultado = $this->conexion->select("SELECT cod FROM familia");
        $familias = (count($resultado) > 0) ? $resultado : array();
        return $familias;
    }

    /**
     * Función que devuelve los productos de una familia
     *
     * @param string $codigo código de la família
     * 
     * @return array array que contiene los productos
     */
    public function getProductosFamilia($codigo){
        $resultado = $this->conexion->select("SELECT cod FROM producto WHERE familia = '$codigo'");
        $productos = (count($resultado) > 0) ? $resultado : array();
        return $productos;
    }

}